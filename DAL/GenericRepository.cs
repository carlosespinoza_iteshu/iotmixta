﻿using COMMON.Entidades;
using COMMON.Interfaces;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DAL
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseDTO
    {
        private MongoClient client;
        private IMongoDatabase db;
        public GenericRepository()
        {
            client = new MongoClient(new MongoUrl(@"mongodb://cespinoza:12345678IoT@ds042138.mlab.com:42138/iotclass"));
            db = client.GetDatabase("iotclass");
            

        }

        private IMongoCollection<T> Collection()
        {
            return db.GetCollection<T>(typeof(T).Name);
        }

        public IQueryable<T> Read => Collection().AsQueryable();

        public T Create(T entidad)
        {
            entidad.Id = ObjectId.GenerateNewId().ToString();
            entidad.FechaHora = DateTime.Now;
            try
            {
                Collection().InsertOne(entidad);
                return entidad;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                return Collection().DeleteOne(m => m.Id == id).DeletedCount == 1;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public IQueryable<T> Query(Expression<Func<T, bool>> predicado)
        {
            try
            {
                return Collection().Find(predicado).ToList().AsQueryable();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public T SearchById(string id)
        {
            try
            {
                return Collection().Find(p => p.Id == id).SingleOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public T Update(T entidad)
        {
            try
            {
                entidad.FechaHora = DateTime.Now;
                return Collection().ReplaceOne(m => m.Id == entidad.Id, entidad).ModifiedCount == 1 ? entidad : null;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}

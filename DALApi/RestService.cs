﻿using COMMON.Entidades;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DALApi
{
    public class RestService<T> where T:BaseDTO 
    {
        private HttpClient cliente;
        private string uriApi;
        public RestService(string baseAddress, string uriApi)
        {
            this.uriApi = uriApi;
            cliente = new HttpClient();
            cliente.BaseAddress = new Uri(baseAddress);
            cliente.DefaultRequestHeaders.Clear();
            cliente.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
            cliente.MaxResponseContentBufferSize = 256000;
            
        }

        public async Task<IEnumerable<T>> ObtenerDatos()
        {
            HttpResponseMessage reposponse = await cliente.GetAsync(uriApi).ConfigureAwait(false);
            if (reposponse.IsSuccessStatusCode)
            {
                var content = await reposponse.Content.ReadAsStringAsync().ConfigureAwait(false);
                var items = JsonConvert.DeserializeObject<IEnumerable<T>>(content);
                return items;
            }
            else
            {
                return null;
            }
        }


        public async Task<T> ObtenerUno(string id)
        {
            HttpResponseMessage response = null;
            response = await cliente.GetAsync(string.Format("{0}{1}", uriApi, id)).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var respuesta = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var item = JsonConvert.DeserializeObject<T>(respuesta);
                return item;
            }
            else
            {
                return null;
            }
        }

        public async Task<T> Guardar(T entidad)
        {
            var json = JsonConvert.SerializeObject(entidad);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = null;
            response = await cliente.PostAsync(uriApi, content).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var respuesta = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var item = JsonConvert.DeserializeObject<T>(respuesta);
                return item;
            }
            else
            {
                return null;
            }
        }
        public async Task<T> Actualizar(T entidad)
        {
            var json = JsonConvert.SerializeObject(entidad);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = null;
            response = await cliente.PutAsync(uriApi, content).ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                var respuesta = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var item = JsonConvert.DeserializeObject<T>(respuesta);
                return item;
            }
            else
            {
                return null;
            }
        }
        public async Task<bool> Eliminar(string id)
        {
            HttpResponseMessage response = null;
            response = await cliente.DeleteAsync(string.Format("{0}{1}", uriApi, id)).ConfigureAwait(false);
            return response.IsSuccessStatusCode;
        }
    }
}

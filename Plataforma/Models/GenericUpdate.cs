﻿using COMMON.Entidades;
using COMMON.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plataforma.Models
{
    public abstract class GenericUpdate<T>:PageModel where T:BaseDTO
    {
        private IGenericRepository<T> repository;
        [BindProperty]
        public T Item { get;set; }
        public GenericUpdate(IGenericRepository<T> repo)
        {
            repository = repo;
        }

        public IActionResult OnGet(string id)
        {
            try
            {
                Item = repository.SearchById(id);
                return Page();
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        public IActionResult OnPost()
        {
            try
            {
                repository.Update(Item);
                return RedirectToPage($"/{typeof(T).Name}/Index");
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}

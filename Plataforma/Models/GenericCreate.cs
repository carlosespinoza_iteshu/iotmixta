﻿using COMMON.Entidades;
using COMMON.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plataforma.Models
{
    public abstract class GenericCreate<T>: PageModel where T : BaseDTO
    {
        private IGenericRepository<T> repository;
        [BindProperty]
        public T Item { get; set; }
        public GenericCreate(IGenericRepository<T> repo)
        {
            repository = repo;
        }
        public IActionResult OnPost()
        {
            try
            {
                repository.Create(Item);
                return RedirectToPage($"/{typeof(T).Name}/Index");
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
    }
}

﻿using COMMON.Entidades;
using COMMON.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plataforma.Models
{
    public abstract class GenericIndex<T> : PageModel where T : BaseDTO
    {
        public List<T> Items { get; private set; }
        private IGenericRepository<T> repository;
        public GenericIndex(IGenericRepository<T> repo)
        {
            repository = repo;
            Items = new List<T>();
        }
        public IActionResult OnGet()
        {
            Items = repository.Read.ToList();
            if (Items != null)
            {
                return Page();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
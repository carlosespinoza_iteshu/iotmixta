﻿using COMMON.Entidades;
using COMMON.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Plataforma.Models
{
    public abstract class GenericDelete<T>:PageModel where T:BaseDTO
    {
        private IGenericRepository<T> repository;
        [BindProperty]
        public T Item { get; set; }
        public GenericDelete(IGenericRepository<T> repo)
        {
            repository = repo;
        }
        public IActionResult OnGet(string id)
        {
            Item = repository.SearchById(id);
            return Page();
        }

        public IActionResult OnPost()
        {
            try
            {
                if (repository.Delete(Item.Id))
                {
                    return RedirectToPage($"/{typeof(T).Name}/Index");
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using COMMON.Entidades;
using COMMON.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace Plataforma.Api
{
    //[Route("api/[controller]")]
    //[ApiController]
    public abstract class GenericController<T> : ControllerBase where T:BaseDTO
    {
        IGenericRepository<T> repository;
        // GET: api/Generic
        public GenericController(IGenericRepository<T> repository)
        {
            this.repository = repository;
        }
        [HttpGet]
        public IEnumerable<T> Get()
        {
            return repository.Read;
        }

        // GET: api/Generic/5
        [HttpGet("{id}")]
        public T Get(string id)
        {
            return repository.SearchById(id);
        }

        // POST: api/Generic
        [HttpPost]
        public T Post([FromBody] T value)
        {
            return repository.Create(value);
        }

        // PUT: api/Generic/5
        [HttpPut("{id}")]
        public T Put([FromBody] T value)
        {
            return repository.Update(value);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public bool Delete(string id)
        {
            return repository.Delete(id);
        }
    }
}

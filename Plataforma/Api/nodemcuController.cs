﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using COMMON.Entidades;
using COMMON.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Plataforma.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class nodemcuController : ControllerBase
    {
        IGenericRepository<Lectura> repository;
        public nodemcuController(IGenericRepository<Lectura> repo)
        {
            repository = repo;
        }
        [HttpGet]
        public IActionResult Get()
        {
            return Ok();
        }
        [HttpPost]
        // POST: api/nodemcu
        public IActionResult Post(string value)
        {
            try
            {
                string[] datos = value.Split(';');
                Lectura l = repository.Create(new Lectura()
                {
                    IdDispositivo = datos[0],
                    Temperatura = float.Parse(datos[1]),
                    Humedad = float.Parse(datos[2]),
                    Luminosidad = float.Parse(datos[3]),
                    Pulsador = int.Parse(datos[4])
                });
                if (l != null)
                {
                    return Ok(l);
                }
                else
                {
                    return BadRequest("Error al guardar la lectura");
                }
            }
            catch (Exception)
            {
                return BadRequest("Ha ocurrido un error");
            }
            
           
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using COMMON.Entidades;
using COMMON.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plataforma.Models;

namespace Plataforma.Pages.Lectura
{
    public class IndexModel : GenericIndex<COMMON.Entidades.Lectura>
    {
        public IndexModel(IGenericRepository<COMMON.Entidades.Lectura> repo) : base(repo)
        {
        }
    }
}
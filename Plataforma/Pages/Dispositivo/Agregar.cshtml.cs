﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using COMMON.Entidades;
using COMMON.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Plataforma.Models;

namespace Plataforma.Pages.Dispositivo
{
    public class AgregarModel : GenericCreate<COMMON.Entidades.Dispositivo>
    {
        public AgregarModel(IGenericRepository<COMMON.Entidades.Dispositivo> repo) : base(repo)
        {
        }
    }
}
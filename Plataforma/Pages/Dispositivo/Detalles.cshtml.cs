﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using COMMON.Entidades;
using COMMON.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MongoDB.Bson;

namespace Plataforma.Pages.Dispositivo
{
    public class DetallesModel : PageModel
    {
        IGenericRepository<COMMON.Entidades.Dispositivo> repositoryDispositivos;
        IGenericRepository<COMMON.Entidades.Lectura> repositoryLecturas;
        IGenericRepository<COMMON.Entidades.Comando> repositoryComandos;
        public COMMON.Entidades.Dispositivo Dispositivo { get; set; }
        public List<COMMON.Entidades.Lectura> Lecturas { get; private set; }
        public List<Comando> ComandosEnviados { get; private set; }
        public List<Comando> ComandosRecibidos { get; private set; }
        public DetallesModel(IGenericRepository<COMMON.Entidades.Dispositivo> repoDispositivos, IGenericRepository<COMMON.Entidades.Lectura> repoLecturas, IGenericRepository<Comando> repoComandos)
        {
            repositoryDispositivos = repoDispositivos;
            repositoryLecturas = repoLecturas;
            repositoryComandos = repoComandos;
            
        }
        public void OnGet(string id)
        {
            Dispositivo = repositoryDispositivos.SearchById(id);
            Lecturas = repositoryLecturas.Query(l => l.IdDispositivo == Dispositivo.Id).OrderBy(f=>f.FechaHora).ToList();
            ComandosEnviados = repositoryComandos.Query(c=>c.IdDispositivoOrigen == Dispositivo.Id).OrderBy(f => f.FechaHora).ToList();
            ComandosRecibidos = repositoryComandos.Query(c => c.IdDispositivoDestino == Dispositivo.Id).OrderBy(f => f.FechaHora).ToList();
        }
    }
}
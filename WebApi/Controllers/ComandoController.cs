﻿using COMMON.Entidades;
using COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    public class ComandoController : GenericApi<Comando>
    {
        public ComandoController() : base(new GenericRepository<Comando>())
        {
        }
    }
}

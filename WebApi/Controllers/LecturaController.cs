﻿using COMMON.Entidades;
using COMMON.Interfaces;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    public class LecturaController : GenericApi<Lectura>
    {
        public LecturaController() : base(new GenericRepository<Lectura>())
        {
        }
    }
}

﻿using COMMON.Entidades;
using COMMON.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    public abstract class GenericApi<T> : ApiController where T:BaseDTO
    {
        private IGenericRepository<T> repository;
        public GenericApi(IGenericRepository<T> repo)
        {
            repository = repo;
        }
        // GET: api/Generic
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(repository.Read);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        //// GET: api/Generic/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/Generic
        public IHttpActionResult Post([FromBody]T value)
        {
            try
            {
                var r = repository.Create(value);
                if (r != null)
                    return Ok(r);
                else
                    return BadRequest("No se pudo crear el objeto");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        //// PUT: api/Generic/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE: api/Generic/5
        //public void Delete(int id)
        //{
        //}
    }
}

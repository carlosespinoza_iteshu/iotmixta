﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Modelos
{
    public class Punto
    {
        public float X { get; set; }
        public string ValorIndependiente { get; set; }
        public float Y { get; set; }
        public bool IndependienteEsNumero { get;private set; }
        public Punto(float x, float y)
        {
            X = x;
            Y = y;
            IndependienteEsNumero = true;
        }
        public Punto(string valorIndependiente,float y)
        {
            ValorIndependiente = valorIndependiente;
            Y = y;
            IndependienteEsNumero = false;
        }

        public override string ToString()
        {
            return "(" + (IndependienteEsNumero ? X.ToString() : ValorIndependiente) + $",{Y})";
        }
    }
}

﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Entidades
{
    public class Comando:BaseDTO
    {
        public string IdDispositivoDestino { get; set; }
        public string IdDispositivoOrigen { get; set; }
        public string ComandoTexto { get; set; }
    }
}

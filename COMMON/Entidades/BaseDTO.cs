﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace COMMON.Entidades
{
    public abstract class BaseDTO:IDisposable
    {
        public string Id { get; set; }
        public DateTime FechaHora { get; set; }
        private bool isDisposed;
        public void Dispose()
        {
            if (!isDisposed)
            {
                isDisposed = true;
                GC.SuppressFinalize(this);
            }
        }
        
    }
}

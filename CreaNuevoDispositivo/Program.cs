﻿using COMMON.Entidades;
using COMMON.Interfaces;
using DALApi;
using System;

namespace CreaNuevoDispositivo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Creando Nuevo Dispositivo");
            Dispositivo d = new Dispositivo();
            Console.Write("Nombre del Dispositivo: ");
            string nombre = Console.ReadLine();
            Console.Write("Contraseña: ");
            string pass = Console.ReadLine();
            d.Nombre = nombre;
            d.Password = pass;
            try
            {
                IGenericRepository<Dispositivo> repositorio = new GenericRepository<Dispositivo>();
                Dispositivo r = repositorio.Create(d);
                if (r != null)
                {
                    Console.WriteLine("Dispositivo Correctamente creado con id:" + r.Id);
                }
                else
                {
                    Console.WriteLine("No se pudo crear el dispositivo...");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }
            Console.WriteLine("Presione una tecla para terminar-...");
            Console.ReadLine();
        }
    }
}

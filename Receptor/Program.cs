﻿using COMMON.Entidades;
using COMMON.Interfaces;
using DAL;
using MongoDB.Bson;
using System;

namespace Receptor
{
    class Program
    {
        static MQTTService mqtt;
        static void Main(string[] args)
        {
            //string servidorMqtt = "m14.cloudmqtt.com";
            //string userMqtt = "dhsjexww";
            //string passMqtt = "SwftHxNnxyXE";
            //int puertoMqtt = 18078;
            string servidorMqtt = "broker.hivemq.com";
            int puertoMqtt = 1883;

            string dispositivoOrigen = "";
            string dispositivoDestino = "5c7aa75f2d4e827b68c959bc";

            string clienteMqtt = "ServerMQTT";
            Console.WriteLine("Servidor MQTT a MongoDB\n\n");
            //mqtt = new MQTTService(servidorMqtt, puertoMqtt, userMqtt, passMqtt, clienteMqtt);
            mqtt = new MQTTService(servidorMqtt, puertoMqtt, clienteMqtt);
            if (mqtt.Conectado)
            {
                mqtt.Suscribir("ISC8MIXTA/#");
                mqtt.MensajeRecibido += Mqtt_MensajeRecibido;
                Console.WriteLine("Escribe el comando a enviar; da enter para terminar");
                string mensaje = "";
                do
                {
                    Console.WriteLine("Comando: ");
                    mensaje = Console.ReadLine();
                    if (mensaje != "")
                    {
                        mqtt.Publicar("ISC8MIXTA/"+dispositivoDestino.ToString(), mensaje);
                        IGenericRepository<Comando> repository = new GenericRepository<Comando>();
                        if(repository.Create(new Comando()
                        {
                             ComandoTexto=mensaje,
                             IdDispositivoDestino=dispositivoDestino,
                             IdDispositivoOrigen=dispositivoOrigen
                        }) != null)
                        {
                            Console.WriteLine("Almacenado");
                        }
                        else
                        {
                            Console.WriteLine("NO Almacenado");
                        }

                    }
                } while (mensaje!="");
                Console.WriteLine("Programa Terminado");
            }
            else
            {
                Console.WriteLine("No se pudo conectar al servidor MQTT...   :(");
            }
            Console.WriteLine("Presiona enter para terminar");
            Console.ReadLine();
        }

        private static void Mqtt_MensajeRecibido(object sender, string e)
        {
            Console.WriteLine("<-" + e);
            //Comandos: B0,B1,L0,L1,AS,S,M<num>
            //Lecturas: <iddispos>;<Sensores>;.....
            if (e.Length > 6)
            {
                //Lectura: 5c7178a6ffd8eb4fd8085929;Sensores;nan;nan;41.00
                string[] datos = e.Split(';');
                if (datos.Length > 4)
                {
                    switch (datos[1])
                    {
                        case "Sensores":
                            Lectura l = new Lectura()
                            {
                                IdDispositivo = datos[0],
                                Temperatura = float.Parse(datos[2]),
                                Humedad = float.Parse(datos[3]),
                                Luminosidad = float.Parse(datos[4])
                            };
                            IGenericRepository<Lectura> repository = new GenericRepository<Lectura>();
                            if (repository.Create(l) == null)
                            {
                                Console.WriteLine("Error al ingresar la lectura.");
                            }
                            else
                            {
                                Console.WriteLine("Lectura ingresada.");
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}
